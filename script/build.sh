#!/bin/bash
# PWD should be the root dir of the repo.
# Build will be placed in bin/ & a tar.gz package built.
#
# You must have go >= 1.13 installed & yarn.
#

if [[ -d "bin" ]]; then
  rm -rf bin/
fi
mkdir bin
mkdir bin/static

cp pigur.db bin/

(
  cd be || exit
  go build -o ../bin/pigur cmd/pigur.go
)

(
  cd fe || exit
  quasar build
  mv dist/spa/* ../bin/static
)
