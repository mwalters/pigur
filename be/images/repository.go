package images

import (
	"database/sql"
	"errors"
	"github.com/google/uuid"
	"time"
)

type Repository interface {
	Delete(id uuid.UUID) (err error)
	New(id uuid.UUID, extension string, image []byte, thumb []byte) (err error)
	Get(id uuid.UUID) (img Image, err error)
	GetAll(lastID uuid.UUID, limit int) (img []Image, err error)
}

func NewSQLiteRepository(con *sql.DB) (Repository, error) {
	r := &sqliteRepository{con: con}
	var err error
	r.deleteQuery, err = con.Prepare("DELETE FROM images WHERE ID = ?")
	if err != nil {
		return r, err
	}
	r.insertQuery, err = con.Prepare("INSERT INTO images (id, created_at, extension, full_file, thumb) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		return r, err
	}
	r.getQuery, err = con.Prepare("SELECT id, extension, full_file, thumb FROM images WHERE ID = ?")
	if err != nil {
		return r, err
	}
	r.getManyWithLastIDQuery, err = con.Prepare(`
		SELECT id, extension, full_file, thumb
		FROM images
		WHERE created_at <= (SELECT created_at
			FROM images
			WHERE id = ?)
		AND ID <> ?
		ORDER BY created_at DESC
		LIMIT ?
	`)
	if err != nil {
		return r, err
	}
	r.getMany, err = con.Prepare(`
		SELECT id, extension, full_file, thumb
		FROM images
		ORDER BY created_at DESC
		LIMIT ?
	`)
	if err != nil {
		return r, err
	}
	return r, nil
}

type sqliteRepository struct {
	con                    *sql.DB
	deleteQuery            *sql.Stmt
	insertQuery            *sql.Stmt
	getQuery               *sql.Stmt
	getManyWithLastIDQuery *sql.Stmt
	getMany                *sql.Stmt
}

func (r sqliteRepository) Delete(id uuid.UUID) (err error) {
	err = transact(r.con, func(tx *sql.Tx) error {
		m := &imageDBModel{ID: id}
		return m.Delete(tx.Stmt(r.deleteQuery))
	})
	return
}

func (r sqliteRepository) Get(id uuid.UUID) (img Image, err error) {
	m := &imageDBModel{}
	err = transact(r.con, func(tx *sql.Tx) error {
		return m.Get(tx.Stmt(r.getQuery), id)
	})
	img, err = r.marshalImageDomain(m)
	return
}

func (r sqliteRepository) GetAll(lastID uuid.UUID, limit int) (img []Image, err error) {
	m := make(imagesDBModel, 0)
	err = transact(r.con, func(tx *sql.Tx) error {
		if lastID != uuid.Nil {
			return m.GetWithLastID(tx.Stmt(r.getManyWithLastIDQuery), lastID, limit)
		} else {
			return m.Get(tx.Stmt(r.getMany), limit)
		}
	})
	if err != nil {
		return img, err
	}
	img, err = r.marshalImagesDomain(m)
	return
}

func (r sqliteRepository) New(id uuid.UUID, extension string, image []byte, thumb []byte) (err error) {
	m := &imageDBModel{
		ID:        id,
		CreatedAt: time.Now().UnixNano(),
		Extension: extension,
		Full:      image,
		Thumb:     thumb,
	}
	err = transact(r.con, func(tx *sql.Tx) error {
		return m.Insert(tx.Stmt(r.insertQuery))
	})
	return
}

func (r sqliteRepository) marshalImageDomain(v *imageDBModel) (img Image, err error) {
	if v.ID == uuid.Nil {
		err = errors.New("id cannot be nil")
		return
	}
	img.ID = v.ID
	img.Extension = v.Extension
	img.Full = v.Full
	img.Thumb = v.Thumb
	return
}

func (r sqliteRepository) marshalImagesDomain(s imagesDBModel) (img []Image, err error) {
	for _, v := range s {
		d := Image{}
		d, err = r.marshalImageDomain(&v)
		if err != nil {
			return
		}
		img = append(img, d)
	}
	return
}

type imageDBModel struct {
	ID        uuid.UUID
	CreatedAt int64
	Extension string
	Full      []byte
	Thumb     []byte
}

func (m *imageDBModel) Delete(stmt *sql.Stmt) error {
	_, err := stmt.Exec(m.ID)
	return err
}

func (m *imageDBModel) Insert(stmt *sql.Stmt) error {
	_, err := stmt.Exec(m.ID, m.CreatedAt, m.Extension, m.Full, m.Thumb)
	return err
}

func (m *imageDBModel) Get(stmt *sql.Stmt, id uuid.UUID) error {
	row := stmt.QueryRow(id)
	return m.loadFromRow(row)
}

func (m *imageDBModel) loadFromRow(row *sql.Row) error {
	return row.Scan(&m.ID, &m.Extension, &m.Full, &m.Thumb)
}

type imagesDBModel []imageDBModel

func (m *imagesDBModel) Get(stmt *sql.Stmt, limit int) error {
	rows, err := stmt.Query(limit)
	defer rows.Close()
	if err != nil {
		return err
	}
	return m.loadFromRows(rows)
}

func (m *imagesDBModel) GetWithLastID(stmt *sql.Stmt, lastID uuid.UUID, limit int) error {
	rows, err := stmt.Query(lastID, lastID, limit)
	defer rows.Close()
	if err != nil {
		return err
	}
	return m.loadFromRows(rows)
}

func (m *imagesDBModel) loadFromRows(rows *sql.Rows) error {
	for rows.Next() {
		v := imageDBModel{}
		if err := rows.Scan(&v.ID, &v.Extension, &v.Full, &v.Thumb); err != nil {
			return err
		}
		*m = append(*m, v)
	}
	return rows.Err()
}

func transact(con *sql.DB, txFunc func(*sql.Tx) error) (err error) {
	tx, err := con.Begin()
	if err != nil {
		return
	}
	defer func() {
		if p := recover(); p != nil {
			tx.Rollback()
			panic(p)
		} else if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit()
		}
	}()
	err = txFunc(tx)
	return
}
