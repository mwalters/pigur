package images

import "github.com/google/uuid"

type Image struct {
	ID        uuid.UUID
	Extension string
	Full      []byte
	Thumb     []byte
}
