package images

import (
	"bytes"
	"github.com/gabriel-vasile/mimetype"
	"github.com/google/uuid"
	"github.com/nfnt/resize"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"io/ioutil"
)

type Service interface {
	Delete(id uuid.UUID) (err error)
	Get(id uuid.UUID) (img Image, err error)
	GetAll(lastID uuid.UUID, limit int) (img []Image, err error)
	New(imageBytes []byte) (id uuid.UUID, err error)
}

func NewService(r Repository) Service {
	return &service{r}
}

type service struct {
	r Repository
}

func (s service) Delete(id uuid.UUID) (err error) {
	return s.r.Delete(id)
}

func (s service) Get(id uuid.UUID) (img Image, err error) {
	return s.r.Get(id)
}

func (s service) GetAll(lastID uuid.UUID, limit int) (img []Image, err error) {
	return s.r.GetAll(lastID, limit)
}

func (s service) New(imageBytes []byte) (id uuid.UUID, err error) {
	var img image.Image
	img, _, err = image.Decode(bytes.NewBuffer(imageBytes))
	if err != nil {
		return
	}
	thumb := resize.Resize(160, 0, img, resize.Lanczos3)
	extension := mimetype.Detect(imageBytes)
	thumbBuf := &bytes.Buffer{}
	err = jpeg.Encode(thumbBuf, thumb, nil)
	if err != nil {
		return
	}
	thumbBytes := make([]byte, 0)
	thumbBytes, err = ioutil.ReadAll(thumbBuf)
	id = uuid.New()
	err = s.r.New(id, extension.String(), imageBytes, thumbBytes)
	return
}
