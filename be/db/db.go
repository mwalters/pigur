package db

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/mwalters/pigur/config"
)

func CreateCon() (*sql.DB, error) {
	con, err := sql.Open("sqlite3", config.DBFile)
	return con, err
}
