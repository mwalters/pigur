package main

import (
	"gitlab.com/mwalters/pigur/db"
	"gitlab.com/mwalters/pigur/server"
)

func main() {
	con, err := db.CreateCon()
	if err != nil {
		panic(err)
	}
	srv := server.New(":8000", con)
	if err := srv.HTTPServer.ListenAndServe(); err != nil {
		panic(err)
	}
}
