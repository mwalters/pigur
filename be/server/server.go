package server

import (
	"database/sql"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/mwalters/pigur/images"
	"gitlab.com/mwalters/pigur/server/web"
	"gitlab.com/mwalters/pigur/server/web/mw"
	"net/http"
	"os"
	"time"
)

type Server struct {
	HTTPServer *http.Server
}

func New(addr string, db *sql.DB) *Server {
	sqliteRepo, err := images.NewSQLiteRepository(db)
	if err != nil {
		panic(err)
	}
	imgSvc := images.NewService(sqliteRepo)
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.RequestID)
	r.Use(mw.SendRequestID)
	r.Use(middleware.Recoverer)
	r.Route("/api", func(api chi.Router) {
		imgRoutes := web.NewImagesRouter(imgSvc)
		api.NotFound(web.APINotFoundHandler)
		api.Use(mw.ResponseMiddleware)
		api.Mount("/images", imgRoutes)
	})
	r.Route("/files", func(files chi.Router) {
		imgFileRoutes := web.NewImageFilesRouter(imgSvc)
		files.Mount("/images", imgFileRoutes)
	})
	// Static files
	r.Get("/*", func(w http.ResponseWriter, r *http.Request) {
		root := "./static"
		fs := http.FileServer(http.Dir(root))
		if _, err := os.Stat(root + r.RequestURI); os.IsNotExist(err) {
			http.StripPrefix(r.RequestURI, fs).ServeHTTP(w, r)
		} else {
			fs.ServeHTTP(w, r)
		}
	})
	s := &Server{&http.Server{
		Addr:         addr,
		Handler:      r,
		ReadTimeout:  20 * time.Second,
		WriteTimeout: 20 * time.Second,
	}}
	return s
}
