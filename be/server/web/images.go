package web

import (
	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/labstack/gommon/log"
	"gitlab.com/mwalters/pigur/images"
	"gitlab.com/mwalters/pigur/server/web/response"
	"io/ioutil"
	"net/http"
)

func NewImagesRouter(s images.Service) *chi.Mux {
	h := imagesHandler{s}
	r := chi.NewRouter()
	r.Get("/{id}", h.GetImage)
	r.Get("/", h.GetImages)
	r.Post("/", h.Post)
	return r
}

type imagesHandler struct {
	s images.Service
}

func (h imagesHandler) GetImage(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := uuid.Parse(idStr)
	if err != nil {
		write(r, http.StatusBadRequest, response.Result{
			Status:  http.StatusText(http.StatusBadRequest),
			Message: "invalid id, must be a valid uuid",
		})
		return
	}
	img, err := h.s.Get(id)
	if err != nil {
		write(r, http.StatusInternalServerError, response.Result{
			Status:  http.StatusText(http.StatusInternalServerError),
			Message: "oops something went wrong",
		})
		return
	}
	resp := response.Get{
		ID:        img.ID,
		Extension: img.Extension,
		Full:      img.Full,
	}
	write(r, http.StatusOK, resp)
}

func (h imagesHandler) GetImages(w http.ResponseWriter, r *http.Request) {
	var lastID uuid.UUID
	var err error
	lastIDStr := r.URL.Query().Get("lastID")
	if lastIDStr != "" {
		lastID, err = uuid.Parse(lastIDStr)
		if err != nil {
			write(r, http.StatusBadRequest, response.Result{
				Status:  http.StatusText(http.StatusBadRequest),
				Message: "invalid lastID, must be a valid UUID",
			})
			return
		}
	} else {
		lastID = uuid.Nil
	}
	limit := 50
	imgs, err := h.s.GetAll(lastID, limit)
	if err != nil {
		write(r, http.StatusInternalServerError, response.Result{
			Status:  http.StatusText(http.StatusInternalServerError),
			Message: "oops something went wrong",
		})
		return
	}
	resp := make(response.GetAll, 0)
	for _, v := range imgs {
		resp = append(resp, response.GetAllItem{
			ID:        v.ID,
			Extension: v.Extension,
			Thumb:     v.Thumb,
		})
	}
	write(r, http.StatusOK, resp)
}

func (h imagesHandler) Post(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(50 * 1024)
	if err != nil {
		write(r, http.StatusBadRequest, response.Result{
			Status:  http.StatusText(http.StatusBadRequest),
			Message: "invalid form data",
		})
		return
	}
	resp := make([]response.Post, 0)
	files := r.MultipartForm.File["files"]
	for _, v := range files {
		f, err := v.Open()
		if err != nil {
			log.Error(err)
			write(r, http.StatusBadRequest, response.Result{
				Status:  http.StatusText(http.StatusBadRequest),
				Message: "error processing files",
			})
			f.Close()
			return
		}
		fBytes, err := ioutil.ReadAll(f)
		id, err := h.s.New(fBytes)
		if err != nil {
			log.Error(err)
			write(r, http.StatusInternalServerError, response.Result{
				Status:  http.StatusText(http.StatusInternalServerError),
				Message: "oops something went wrong",
			})
			f.Close()
			return
		}
		resp = append(resp, response.Post{ID: id})
		f.Close()
	}
	write(r, http.StatusCreated, resp)
}

func NewImageFilesRouter(s images.Service) *chi.Mux {
	h := imageFilesHandler{s}
	r := chi.NewRouter()
	r.Get("/{id}", h.Get)
	return r
}

type imageFilesHandler struct {
	s images.Service
}

func (h imageFilesHandler) Get(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := uuid.Parse(idStr)
	if err != nil {
		http.Error(w, "invalid id, must be a valid uuid", http.StatusBadRequest)
		return
	}
	img, err := h.s.Get(id)
	if err != nil {
		http.Error(w, "oops something went wrong", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(img.Full)
}
