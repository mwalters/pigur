package response

import "github.com/google/uuid"

type Get struct {
	ID        uuid.UUID `json:"id" xml:"id"`
	Extension string    `json:"extension" xml:"extension"`
	Full      []byte    `json:"full" xml:"full"`
}

type GetAll []GetAllItem

type GetAllItem struct {
	ID        uuid.UUID `json:"id" xml:"id"`
	Extension string    `json:"extension"`
	Thumb     []byte    `json:"thumb" xml:"thumb"`
}

type Post struct {
	ID uuid.UUID `json:"id" xml:"id"`
}
