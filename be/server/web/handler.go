package web

import (
	"gitlab.com/mwalters/pigur/server/web/response"
	"net/http"
)

func APINotFoundHandler(w http.ResponseWriter, r *http.Request) {
	write(r, http.StatusNotFound, response.Result{
		Status:  http.StatusText(http.StatusNotFound),
		Message: "not found",
	})
}
