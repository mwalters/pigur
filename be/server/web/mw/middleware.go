package mw

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"gitlab.com/mwalters/pigur/server/web/response"
	"net/http"

	"github.com/go-chi/chi/v5/middleware"
)

type contextKey string

const (
	StatusCodeKey     contextKey = "http_status_code"
	ResponseStructKey contextKey = "http_response_body"
	JWTSecretKey      contextKey = "jwt_secret_key"
	UserStructKey     contextKey = "user"
)

type ContentEncoder interface {
	Encode(v interface{}) error
}

func ResponseMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		accept := r.Header.Get("Accept")
		var encoder ContentEncoder

		switch accept {
		case "application/xml":
			w.Header().Set("Content-Type", "application/xml")
			encoder = xml.NewEncoder(w)
		case "application/json":
			w.Header().Set("Content-Type", "application/json")
			encoder = json.NewEncoder(w)
		case "*/*":
			w.Header().Set("Content-Type", "application/json")
			encoder = json.NewEncoder(w)
		case "":
			w.Header().Set("Content-Type", "application/json")
			encoder = json.NewEncoder(w)
		default:

			w.WriteHeader(http.StatusUnsupportedMediaType)
			w.Write([]byte{})
			if r.Body != nil {
				r.Body.Close()
			}
			return
		}

		next.ServeHTTP(w, r)

		if status, ok := r.Context().Value(StatusCodeKey).(int); ok {
			w.WriteHeader(status)
			encoder.Encode(r.Context().Value(ResponseStructKey))
		} else {
			resp := response.Result{
				Status:  http.StatusText(http.StatusInternalServerError),
				Message: "There was an unfortunate error",
			}
			w.WriteHeader(http.StatusInternalServerError)
			encoder.Encode(resp)
		}
	}
	return http.HandlerFunc(fn)
}

func SendRequestID(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add(middleware.RequestIDHeader, middleware.GetReqID(r.Context()))
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func Write(r *http.Request, status int, result interface{}) {
	*r = *r.WithContext(context.WithValue(r.Context(), StatusCodeKey, status))
	*r = *r.WithContext(context.WithValue(r.Context(), ResponseStructKey, result))
}
