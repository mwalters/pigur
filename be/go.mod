module gitlab.com/mwalters/pigur

go 1.13

require (
	github.com/gabriel-vasile/mimetype v1.1.0
	github.com/go-chi/chi/v5 v5.0.7
	github.com/google/uuid v1.3.0
	github.com/labstack/gommon v0.4.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
)
